# Changelog

This changelog is based on [Keep a Changelog 1.0.0](https://keepachangelog.com/en/1.0.0).

This project adheres to [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0).

## [Unreleased]

### Added

- Created `Converter`
- Created `ConverterResolver`
- Created `ConversionException`
- Created `ExactConverterResolver`
- Created `IdentityConverter`
- Created `IdentityConverterResolver`
- Created `AutoboxingConverterResolver`
- Created `ConstructorConverterResolver`
- Created `ToStringConverter`
- Created `ConverterResolverChain`
- Created `CachingConverterResolver`
