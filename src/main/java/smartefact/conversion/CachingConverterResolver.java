/*
 * Copyright 2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.conversion;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.HashCodeBuilder.hashCodeBuilder;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * {@link ConverterResolver} that caches the {@link Converter}s
 * resolved by a wrapped {@code ConverterResolver}
 * by the combination of <i>from-type</i> and <i>to-type</i>.
 *
 * @author Laurent Pireyn
 */
public final class CachingConverterResolver implements ConverterResolver {
    private static final class Key {
        private final @NotNull Type fromType;
        private final @NotNull Type toType;
        private final int hashCode;

        Key(
            @NotNull Type fromType,
            @NotNull Type toType
        ) {
            this.fromType = fromType;
            this.toType = toType;
            hashCode = hashCodeBuilder()
                .property(fromType)
                .property(toType)
                .build();
        }

        @Override
        @Contract(value = "null -> fail", pure = true)
        public boolean equals(@Nullable Object object) {
            if (this == object) {
                return true;
            }
            assert object instanceof Key;
            final @NotNull Key other = (Key) object;
            return fromType.equals(other.fromType)
                && toType.equals(other.toType);
        }

        @Override
        @Contract(pure = true)
        public int hashCode() {
            return hashCode;
        }
    }

    private final @NotNull ConverterResolver resolver;
    private final Map<@NotNull Key, @NotNull Converter<?, ?>> convertersCache = new HashMap<>();

    public CachingConverterResolver(@NotNull ConverterResolver resolver) {
        this.resolver = requireNotNull(resolver);
    }

    @Override
    @SuppressWarnings("unchecked")
    public @Nullable <TFrom, TTo> Converter<TFrom, TTo> resolveConverter(
        @NotNull Type fromType,
        @NotNull Type toType
    ) {
        requireNotNull(fromType);
        requireNotNull(toType);
        final Key key = new Key(fromType, toType);
        return (Converter<TFrom, TTo>) convertersCache.computeIfAbsent(
            key,
            _key -> resolver.resolveConverter(fromType, toType)
        );
    }
}
