/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.conversion;

import java.lang.reflect.Type;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import smartefact.reflect.TypeReference;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * {@link Converter} resolver.
 *
 * @author Laurent Pireyn
 */
public interface ConverterResolver {
    <TFrom, TTo> @Nullable Converter<TFrom, TTo> resolveConverter(
        @NotNull Type fromType,
        @NotNull Type toType
    );

    default <TFrom, TTo> @Nullable Converter<TFrom, TTo> resolveConverter(
        @NotNull Class<TFrom> fromType,
        @NotNull Class<TTo> toType
    ) {
        return resolveConverter((Type) fromType, toType);
    }

    default <TFrom, TTo> @Nullable Converter<TFrom, TTo> resolveConverter(
        @NotNull TypeReference<TFrom> fromType,
        @NotNull TypeReference<TTo> toType
    ) {
        requireNotNull(fromType);
        requireNotNull(toType);
        return resolveConverter(fromType.getType(), toType.getType());
    }
}
