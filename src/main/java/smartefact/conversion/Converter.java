/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.conversion;

import org.jetbrains.annotations.NotNull;

/**
 * Converter.
 *
 * @param <TFrom> the type of <i>from-value</i>
 * @param <TTo> the type of <i>to-value</i>
 * @author Laurent Pireyn
 */
@FunctionalInterface
public interface Converter<TFrom, TTo> {
    /**
     * Converts the given <i>from-value</i> to a <i>to-value</i>.
     *
     * @param from the from-value (must not be {@code null})
     * @return {@code from} converted to a to-value (never {@code null})
     */
    @NotNull TTo convert(@NotNull TFrom from);
}
