/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.conversion;

import org.jetbrains.annotations.Nullable;

/**
 * {@link RuntimeException} thrown by a {@link Converter}.
 *
 * @author Laurent Pireyn
 */
public class ConversionException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ConversionException(@Nullable String message) {
        super(message);
    }

    public ConversionException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }
}
