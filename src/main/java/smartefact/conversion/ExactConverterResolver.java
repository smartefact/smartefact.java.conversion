/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.conversion;

import java.lang.reflect.Type;
import org.jetbrains.annotations.NotNull;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * {@link StaticConverterResolver} that resolves
 * an exact combination of <i>from-type</i> and <i>to-type</i>
 * to its {@link Converter}.
 *
 * @author Laurent Pireyn
 */
public final class ExactConverterResolver<TFrom, TTo> extends StaticConverterResolver {
    private final @NotNull Type supportedFromType;
    private final @NotNull Type supportedToType;

    public ExactConverterResolver(
        @NotNull Converter<TFrom, TTo> converter,
        @NotNull Type supportedFromType,
        @NotNull Type supportedToType
    ) {
        super(converter);
        this.supportedFromType = requireNotNull(supportedFromType);
        this.supportedToType = requireNotNull(supportedToType);
    }

    @Override
    protected boolean supportsType(
        @NotNull Type fromType,
        @NotNull Type toType
    ) {
        return fromType.equals(supportedFromType)
            && toType.equals(supportedToType);
    }
}
