/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.conversion;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import smartefact.util.Classes;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * {@link ConverterResolver} that resolves to a converter
 * that invokes the constructor of the <i>to-type</i>
 * that takes the <i>from-value</i> as single argument.
 *
 * @author Laurent Pireyn
 */
public final class ConstructorConverterResolver implements ConverterResolver {
    private static final class ConstructorConverter<TFrom, TTo> implements Converter<TFrom, TTo> {
        private final @NotNull Constructor<TTo> constructor;

        ConstructorConverter(@NotNull Constructor<TTo> constructor) {
            this.constructor = constructor;
        }

        @Override
        public @NotNull TTo convert(@NotNull TFrom from) {
            requireNotNull(from);
            try {
                return constructor.newInstance(from);
            } catch (InvocationTargetException e) {
                throw new ConversionException("The constructor <" + constructor + "> of the class <" + constructor.getDeclaringClass().getName() + "> threw a <" + e.getClass().getSimpleName() + '>', e);
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException e) {
                throw new AssertionError(e);
            }
        }
    }

    public static final ConstructorConverterResolver INSTANCE = new ConstructorConverterResolver();

    private ConstructorConverterResolver() {}

    @Override
    @SuppressWarnings("unchecked")
    public @Nullable <TFrom, TTo> Converter<TFrom, TTo> resolveConverter(
        @NotNull Type fromType,
        @NotNull Type toType
    ) {
        requireNotNull(fromType);
        requireNotNull(toType);
        return fromType instanceof Class<?> && toType instanceof Class<?>
            ? resolveConverter((Class<TFrom>) fromType, (Class<TTo>) toType)
            : null;
    }

    @Override
    public @Nullable <TFrom, TTo> Converter<TFrom, TTo> resolveConverter(
        @NotNull Class<TFrom> fromType,
        @NotNull Class<TTo> toType
    ) {
        requireNotNull(fromType);
        requireNotNull(toType);
        if (Modifier.isAbstract(toType.getModifiers())) {
            return null;
        }
        final Constructor<TTo> constructor = Classes.getConstructorOrNull(toType, fromType);
        return constructor != null
            ? new ConstructorConverter<>(constructor)
            : null;
    }
}
