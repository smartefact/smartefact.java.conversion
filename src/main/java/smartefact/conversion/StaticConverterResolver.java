/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.conversion;

import java.lang.reflect.Type;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * Abstract {@link ConverterResolver} that resolves to a static {@link Converter}.
 *
 * @author Laurent Pireyn
 */
public abstract class StaticConverterResolver implements ConverterResolver {
    private final @NotNull Converter<?, ?> converter;

    protected StaticConverterResolver(@NotNull Converter<?, ?> converter) {
        this.converter = requireNotNull(converter);
    }

    @Override
    @SuppressWarnings("unchecked")
    public final @Nullable <TFrom, TTo> Converter<TFrom, TTo> resolveConverter(
        @NotNull Type fromType,
        @NotNull Type toType
    ) {
        requireNotNull(fromType);
        requireNotNull(toType);
        return supportsType(fromType, toType)
            ? (Converter<TFrom, TTo>) converter
            : null;
    }

    protected abstract boolean supportsType(
        @NotNull Type fromType,
        @NotNull Type toType
    );
}
