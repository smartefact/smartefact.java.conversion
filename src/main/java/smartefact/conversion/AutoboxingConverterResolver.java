/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.conversion;

import java.lang.reflect.Type;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import smartefact.util.Arrays;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * {@link ConverterResolver} that resolves a {@link Converter} that performs autoboxing.
 *
 * @author Laurent Pireyn
 */
public final class AutoboxingConverterResolver implements ConverterResolver {
    private static final Type[] PRIMITIVE_TYPES = {
        Boolean.TYPE,
        Byte.TYPE,
        Character.TYPE,
        Double.TYPE,
        Float.TYPE,
        Integer.TYPE,
        Long.TYPE,
        Short.TYPE
    };

    private static final Class<?>[] WRAPPER_TYPES = {
        Boolean.class,
        Byte.class,
        Character.class,
        Double.class,
        Float.class,
        Integer.class,
        Long.class,
        Short.class
    };

    public static final AutoboxingConverterResolver INSTANCE = new AutoboxingConverterResolver();

    private static boolean isWrapperClassOf(
        @NotNull Type primitiveType,
        @NotNull Type wrapperType
    ) {
        final int index = Arrays.indexOf(PRIMITIVE_TYPES, primitiveType);
        return index != -1 && WRAPPER_TYPES[index].equals(wrapperType);
    }

    private static boolean supportsTypes(
        @NotNull Type from,
        @NotNull Type to
    ) {
        return isWrapperClassOf(from, to)
            || isWrapperClassOf(to, from);
    }

    private AutoboxingConverterResolver() {}

    @Override
    @SuppressWarnings("unchecked")
    public @Nullable <TFrom, TTo> Converter<TFrom, TTo> resolveConverter(
        @NotNull Type fromType,
        @NotNull Type toType
    ) {
        requireNotNull(fromType);
        requireNotNull(toType);
        return supportsTypes(fromType, toType)
            ? (Converter<TFrom, TTo>) IdentityConverter.instance()
            : null;
    }
}
