/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.conversion;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;

class AutoboxingConverterTest {
    static <T> void checkConversion(Class<T> fromType, Class<T> toType, T value) {
        final Converter<T, T> converter = AutoboxingConverterResolver.INSTANCE.resolveConverter(fromType, toType);
        assertNotNull(converter);
        final Converter<T, T> reverseConverter = AutoboxingConverterResolver.INSTANCE.resolveConverter(toType, fromType);
        assertNotNull(reverseConverter);
        assertEquals(value, converter.convert(value));
        assertEquals(value, reverseConverter.convert(value));
    }

    @Test
    void testConvertBoolean() {
        checkConversion(boolean.class, Boolean.class, true);
    }

    @Test
    void testConvertByte() {
        checkConversion(byte.class, Byte.class, (byte) 1);
    }

    @Test
    void testConvertChar() {
        checkConversion(char.class, Character.class, 'a');
    }

    @Test
    void testConvertDouble() {
        checkConversion(double.class, Double.class, 1.0);
    }

    @Test
    void testConvertFloat() {
        checkConversion(float.class, Float.class, 1.0f);
    }

    @Test
    void testConvertInt() {
        checkConversion(int.class, Integer.class, 1);
    }

    @Test
    void testConvertLong() {
        checkConversion(long.class, Long.class, 1L);
    }

    @Test
    void testConvertShort() {
        checkConversion(short.class, Short.class, (short) 1);
    }
}
