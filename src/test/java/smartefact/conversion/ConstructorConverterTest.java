/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.conversion;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;

class ConstructorConverterTest {
    @Test
    void testConvertFile() {
        final var converter = ConstructorConverterResolver.INSTANCE.resolveConverter(String.class, File.class);
        assertNotNull(converter);
        assertEquals(new File("abc"), converter.convert("abc"));
    }

    @Test
    void testConvertUrl() throws MalformedURLException {
        final var converter = ConstructorConverterResolver.INSTANCE.resolveConverter(String.class, URL.class);
        assertNotNull(converter);
        assertEquals(new URL("http://example.org"), converter.convert("http://example.org"));
    }

    @Test
    void testConvertUri() {
        final var converter = ConstructorConverterResolver.INSTANCE.resolveConverter(String.class, URI.class);
        assertNotNull(converter);
        assertEquals(URI.create("http://example.org"), converter.convert("http://example.org"));
    }
}
