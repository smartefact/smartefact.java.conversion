/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.conversion;

import java.lang.reflect.Type;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import org.junit.jupiter.api.Test;

class AutoboxingConverterResolverTest {
    static final Type[] PRIMITIVE_TYPES = {
        Boolean.TYPE,
        Byte.TYPE,
        Character.TYPE,
        Double.TYPE,
        Float.TYPE,
        Integer.TYPE,
        Long.TYPE,
        Short.TYPE
    };

    static final Type[] WRAPPER_TYPES = {
        Boolean.class,
        Byte.class,
        Character.class,
        Double.class,
        Float.class,
        Integer.class,
        Long.class,
        Short.class
    };

    @Test
    void testResolveConverterSuccess() {
        assertAll(() -> {
            for (int i = 0; i < PRIMITIVE_TYPES.length; ++i) {
                assertNotNull(AutoboxingConverterResolver.INSTANCE.resolveConverter(PRIMITIVE_TYPES[i], WRAPPER_TYPES[i]));
                assertNotNull(AutoboxingConverterResolver.INSTANCE.resolveConverter(WRAPPER_TYPES[i], PRIMITIVE_TYPES[i]));
            }
        });
    }

    @Test
    void testResolveConverterFailure() {
        assertAll(() -> {
            for (int i = 0; i < PRIMITIVE_TYPES.length; ++i) {
                for (int j = 0; j < PRIMITIVE_TYPES.length; ++j) {
                    if (j != i) {
                        assertNull(AutoboxingConverterResolver.INSTANCE.resolveConverter(PRIMITIVE_TYPES[i], PRIMITIVE_TYPES[j]));
                        assertNull(AutoboxingConverterResolver.INSTANCE.resolveConverter(WRAPPER_TYPES[i], WRAPPER_TYPES[j]));
                        assertNull(AutoboxingConverterResolver.INSTANCE.resolveConverter(PRIMITIVE_TYPES[i], WRAPPER_TYPES[j]));
                        assertNull(AutoboxingConverterResolver.INSTANCE.resolveConverter(WRAPPER_TYPES[i], PRIMITIVE_TYPES[j]));
                    }
                }
            }
        });
    }
}
