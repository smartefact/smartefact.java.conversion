[![Latest release](https://gitlab.com/smartefact/smartefact.java.conversion/-/badges/release.svg?key_text=Latest%20release&key_width=100)](https://gitlab.com/smartefact/smartefact.java.conversion/-/releases)
[![Pipeline status](https://gitlab.com/smartefact/smartefact.java.conversion/badges/main/pipeline.svg?key_text=Pipeline%20status&key_width=100)](https://gitlab.com/smartefact/smartefact.java.conversion/-/commits/main)
[![Coverage](https://gitlab.com/smartefact/smartefact.java.conversion/badges/main/coverage.svg?key_text=Coverage&key_width=100)](https://gitlab.com/smartefact/smartefact.java.conversion/-/commits/main)

# smartefact.java.conversion

Conversion utilities in Java.

## Changelog

See [CHANGELOG](CHANGELOG.md).

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md).

## License

This project is licensed under the [Apache License 2.0](LICENSE).
